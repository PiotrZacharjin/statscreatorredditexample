package com.example;

import com.example.attribute.AttributeModifier;
import com.example.attribute.Attributes;

import java.util.List;

public class Dice {

    private List<AttributeModifier> modifier;

    public Dice(List<AttributeModifier> modifier) {
        this.modifier = modifier;
    }

    Attributes reRoll(Attributes attributes){
        Attributes newAttributes = attributes.cpy();
        modifier.forEach(m->m.modify(newAttributes));
        return newAttributes;
    }
}
