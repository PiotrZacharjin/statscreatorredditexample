package com.example;

import com.example.attribute.AttributeModifier;
import com.example.attribute.Attributes;

import java.util.List;

public class Race  implements AttributeModifier {

    private final List<AttributeModifier> modifiers;

    public Race(List<AttributeModifier> modifiers) {
        this.modifiers = modifiers;
    }

    @Override
    public void modify(Attributes attributes) {
        modifiers.forEach(m->m.modify(attributes));
    }
}
