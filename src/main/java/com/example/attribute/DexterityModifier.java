package com.example.attribute;

public class DexterityModifier implements AttributeModifier {

    private ValueProvider value;

    public DexterityModifier(ValueProvider value) {
        this.value = value;
    }

    @Override
    public void modify(Attributes attributes) {
        attributes.increaseDexterity(value.provide());
    }
}
