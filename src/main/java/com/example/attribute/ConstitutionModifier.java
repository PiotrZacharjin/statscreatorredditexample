package com.example.attribute;

public class ConstitutionModifier implements AttributeModifier {

    private ValueProvider value;

    public ConstitutionModifier(ValueProvider value) {
        this.value = value;
    }

    @Override
    public void modify(Attributes attributes) {
        attributes.increaseConstitution(value.provide());
    }
}
