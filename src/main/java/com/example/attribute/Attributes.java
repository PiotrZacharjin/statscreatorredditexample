package com.example.attribute;

public class Attributes {

    private Long strength;
    private Long dexterity;
    private Long constitution;
    private Long intelligence;
    private Long wisdom;
    private Long charisma;

    public Attributes(Long strength, Long dexterity, Long constitution, Long intelligence, Long wisdom, Long charisma) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.constitution = constitution;
        this.intelligence = intelligence;
        this.wisdom = wisdom;
        this.charisma = charisma;
    }

    void increaseStrength(Long strength) {
        this.strength += strength;
    }

    void increaseDexterity(Long dexterity) {
        this.dexterity +=  dexterity;
    }

    void increaseConstitution(Long constitution) {
        this.constitution += constitution;
    }

    void increaseIntelligence(Long intelligence) {
        this.intelligence += intelligence;
    }

     void increaseWisdom(Long wisdom) {
        this.wisdom +=  wisdom;
    }

     void increaseCharisma(Long charisma) {
        this.charisma +=  charisma;
    }

    public Long getStrength() {
        return strength;
    }

    public Long getDexterity() {
        return dexterity;
    }

    public Long getConstitution() {
        return constitution;
    }

    public Long getIntelligence() {
        return intelligence;
    }

    public Long getWisdom() {
        return wisdom;
    }

    public Long getCharisma() {
        return charisma;
    }

    public Attributes cpy() {
       return new Attributes(strength,dexterity,constitution,intelligence,wisdom,charisma);
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", constitution=" + constitution +
                ", intelligence=" + intelligence +
                ", wisdom=" + wisdom +
                ", charisma=" + charisma +
                '}';
    }
}
