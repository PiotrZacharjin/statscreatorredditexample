package com.example.attribute;

public class WisdomModifier implements AttributeModifier {

    private ValueProvider value;

    public WisdomModifier(ValueProvider value) {
        this.value = value;
    }
    @Override
    public void modify(Attributes attributes) {
        attributes.increaseWisdom(value.provide());
    }
}
