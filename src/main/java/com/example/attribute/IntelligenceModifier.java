package com.example.attribute;

public class IntelligenceModifier implements AttributeModifier {

    private ValueProvider value;

    public IntelligenceModifier(ValueProvider value) {
        this.value = value;
    }

    @Override
    public void modify(Attributes attributes) {
        attributes.increaseIntelligence(value.provide());
    }
}
