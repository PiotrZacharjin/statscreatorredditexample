package com.example.attribute;

public interface AttributeModifier {

    void modify(Attributes attributes);
}
