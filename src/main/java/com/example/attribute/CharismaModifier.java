package com.example.attribute;

public class CharismaModifier implements AttributeModifier {

    private ValueProvider value;

    public CharismaModifier(ValueProvider value) {
        this.value = value;
    }

    @Override
    public void modify(Attributes attributes) {
        attributes.increaseCharisma(value.provide());
    }
}
