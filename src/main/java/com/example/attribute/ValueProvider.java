package com.example.attribute;

public interface ValueProvider {

    Long provide();
}
