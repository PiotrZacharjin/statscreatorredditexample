package com.example.attribute;

public class StrengthModifier implements AttributeModifier {

    private ValueProvider value;

    public StrengthModifier(ValueProvider value) {
        this.value = value;
    }

    @Override
    public void modify(Attributes attributes) {
        attributes.increaseStrength(value.provide());
    }
}
