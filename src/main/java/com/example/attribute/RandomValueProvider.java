package com.example.attribute;

public class RandomValueProvider implements ValueProvider {
    private Long min;
    private Long max;

    public RandomValueProvider(Long min, Long max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public Long provide() {
        return Double.valueOf(Math.random() * (max - min + 1) + min).longValue();
    }
}
