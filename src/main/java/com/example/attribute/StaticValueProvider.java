package com.example.attribute;

public class StaticValueProvider implements ValueProvider {

    private Long value;


    public StaticValueProvider(Long value) {
        this.value = value;
    }


    @Override
    public Long provide() {
        return value;
    }
}
