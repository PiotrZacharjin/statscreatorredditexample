package com.example;

import com.example.attribute.*;

import java.util.Arrays;

public class RoleFactory {


    public static Role createMageRole(){
        return new Role(Arrays.asList(
            new IntelligenceModifier(new RandomValueProvider(2L,6L)),
                new StrengthModifier(new RandomValueProvider(4L,6L)),
                new DexterityModifier(new RandomValueProvider(1L,2L))
        ));
    }
}
