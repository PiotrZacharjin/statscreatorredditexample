package com.example;

import com.example.attribute.DexterityModifier;
import com.example.attribute.RandomValueProvider;
import com.example.attribute.StaticValueProvider;

import java.util.Arrays;

public class RaceFactory {
    public static Race createGnome() {
        return new Race(Arrays.asList(
                new DexterityModifier(new StaticValueProvider(1L))));
    }
}
