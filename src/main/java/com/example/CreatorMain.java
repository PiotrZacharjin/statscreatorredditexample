package com.example;

import com.example.attribute.Attributes;
import com.example.attribute.DexterityModifier;
import com.example.attribute.RandomValueProvider;

import java.util.Arrays;

public class CreatorMain {

    public static void main(String[] args){
        Role mageRole = RoleFactory.createMageRole();
        Race gnome = RaceFactory.createGnome();

        Attributes base = new Attributes(4L,0L,0L,1L,0L,0L);

        Attributes attributes = new CharacterAttributeCreator(gnome, mageRole)
                .create(base);

        System.out.println(attributes);

        Dice dice = new Dice(Arrays.asList(new DexterityModifier(new RandomValueProvider(-5L, 5L))));

        Attributes rolled = dice.reRoll(attributes);
        System.out.println(rolled);
    }
}
