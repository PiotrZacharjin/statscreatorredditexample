package com.example;

import com.example.attribute.Attributes;

public class CharacterAttributeCreator {

    private Race race;
    private Role role;

    public CharacterAttributeCreator(Race race, Role role) {
        this.race = race;
        this.role = role;
    }

    Attributes create(Attributes definedAttributes){
        Attributes attributes = definedAttributes.cpy();
        race.modify(attributes);
        role.modify(attributes);
        return attributes;
    }


}
